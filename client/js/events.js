"use strict";

Template.form.events({
    'submit .add-new-task': function (event) {
        //keeps page from refreshing
        event.preventDefault();

        var taskName = event.currentTarget.children[0].firstElementChild.value;
        //if nothing is typed in input, than a popup notifies to write something
        if (taskName === "") {
            Materialize.toast("Please type something before submitting.", 4000);
            return false;
        }
        //insert task into todo list
        Collections.Todo.insert({
            name: taskName,
            createdAt: new Date(),
            complete: false
        });
        event.currentTarget.children[0].firstElementChild.value = "";
        return false;
    }
});

Template.todos.events({
    //when delete button is pressed, item is removed from the database
    'click .delete-task': function (event) {

        Collections.Todo.remove({
            _id: this._id
        });
    },

    //when complete button pressed, shown as finished
    'click .complete-task': function (event) {
        Collections.Todo.update({
            _id: this._id

        }, {
            $set: {
                complete: true
            }
        });
    },
    //when button pressed, shown as not finished
    'click .incomplete-task': function (event) {
        Collections.Todo.update({
            _id: this._id

        }, {
            $set: {
                complete: false
            }
        });
    },
    'click .edit-task': function (event) {
        var edit = prompt("Go ahead edit your task.");
        Collections.Todo.update({
            _id: this._id

        }, {
            $set: {
                name: edit
            }
        });
    }
});